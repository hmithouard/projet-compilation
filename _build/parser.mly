%{
  open Ast
  open Ast.Syntax
%}

%token <int> Lint
%token <string> Lvar
%token Lend Ladd Lsub Lassign Ldecl Lmul Lif Lpo Lpf Lelse Po Pf Lloop Lneqq Leqq Llt Lgt

%start block

%type <Ast.Syntax.block> block

%%

block:
| e = instr; b = block { 
  [e] @ b 
}
| e = instr{ [e] }
| Lend { [] }
;

expr:
| n = Lint {
  Int { value = n ; pos = $startpos(n) }
}
| v = Lvar {
  Var { name = v ; pos = $startpos(v) }
}
| a = expr; Ladd; b = expr {
    Call {  func = "%add";
            args = [a ; b];
            pos = $startpos($2) }
}
| a = expr; Lsub; b = expr {
    Call {  func = "%sub";
            args = [a ; b];
            pos = $startpos($2) }
}
| a = expr; Lmul; b = expr {
    Call {  func = "%mul";
            args = [a ; b];
            pos = $startpos($2) }
}
| a = expr; Lassign; Lassign; b = expr{
    Call { func = "%eqq";
           args = [a ; b];
           pos = $startpos($2)}
}
| a = expr; Leqq; b = expr{
    Call { func = "%eqq";
           args = [a ; b];
           pos = $startpos($2)}
}
| a = expr; Lneqq; b = expr{
    Call { func = "%neqq";
           args = [a ; b];
           pos = $startpos($2)}
}
| a = expr; Llt; b = expr{
    Call { func = "%lesst";
           args = [a ; b];
           pos = $startpos($2)}
}
| a = expr; Lgt; b = expr{
    Call { func = "%grtt";
           args = [a ; b];
           pos = $startpos($2)}
}
;

instr:
| Ldecl; v = Lvar{
  Decl {  var = v
       ;  pos = $startpos(v) 
  }
}
| v = Lvar; Lassign; b = expr{
    Assign {  var = v;
              expr = b;
              pos = $startpos($2)}
}
| Lif; Lpo ; a = expr; Lpf;Po; b = block;Pf; Lelse;Po; c = block; Pf{
    Cond{ cond = a
        ; bif = b
        ; belse = c
        ; pos = $startpos(a)}
}
| Lloop; Lpo ; a = expr; Lpf;Po; b = block;Pf{
    Loop{ fin = a
        ; block = b 
        ; pos = $startpos(a)}
}
