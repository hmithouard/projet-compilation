
(* The type of tokens. *)

type token = 
  | Po
  | Pf
  | Lvar of (string)
  | Lsub
  | Lpo
  | Lpf
  | Lneqq
  | Lmul
  | Llt
  | Lloop
  | Lint of (int)
  | Lif
  | Lgt
  | Leqq
  | Lend
  | Lelse
  | Ldecl
  | Lassign
  | Ladd

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val block: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Ast.Syntax.block)
