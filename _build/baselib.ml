open Ast
open Mips

module Env = Map.Make(String)

let _types_ = Env.of_seq
(List.to_seq
   [ "%add", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%sub", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%mul", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%eqq", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%neqq", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%lesst", Func_t (Int_t, [ Int_t ; Int_t ])
   ; "%grtt", Func_t (Int_t, [ Int_t ; Int_t ])
])

let builtins = 
   [
   Label "_add"
    ;Lw (T0, Mem (SP, 0))
    ;Lw (T1, Mem (SP, 4))
    ;Add (V0, T0, T1)
    ;Jr RA

    ; Label "_sub"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Sub (V0, T0, T1)
    ; Jr RA

    ; Label "_mul"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Mul (V0, T0, T1)
    ; Jr RA

    ; Label "_eqq"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Beq ( T0, T1, "_iseqq")
    ; Li (V0, 1)
    ; Jr RA
    ; Label "_iseqq"
    ; Li (V0 , 0)
    ; Jr RA

    ; Label "_neqq"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Beq ( T0, T1, "_iseqq")
    ; Li (V0, 0)
    ; Jr RA
    ; Label "_iseqq"
    ; Li (V0 , 1)
    ; Jr RA

    ; Label "_lesst"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Slt ( V0, T0, T1)
    ; Jr RA

    ; Label "_grtt"
    ; Lw (T0, Mem (SP, 0))
    ; Lw (T1, Mem (SP, 4))
    ; Slt ( V0, T1, T0)
    ; Jr RA

    


   ]
