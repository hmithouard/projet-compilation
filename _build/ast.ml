type type_t =
  | Int_t
  | Func_t of type_t * type_t list

  let rec string_of_type_t t =
    match t with
    | Int_t  -> "int"
    | Func_t (r, a) ->
       (if (List.length a) > 1 then "(" else "")
       ^ (String.concat ", " (List.map string_of_type_t a))
       ^ (if (List.length a) > 1 then ")" else "")
       ^ " -> " ^ (string_of_type_t r)
module Syntax = struct
  type name = string
  type expr =
    | Int of { value: int
             ; pos: Lexing.position }
    | Var of { name : name
             ; pos : Lexing.position }
    | Call of { func : name 
              ; args : expr list
              ; pos : Lexing.position}
  type instr = 
    | Decl  of {  var : name
               ;  pos : Lexing.position
    }
    | Assign of { var : name
                ; expr : expr
                ; pos : Lexing.position}
    | Cond of { cond : expr
              ; bif : block
              ; belse : block
              ; pos : Lexing.position}
    | Loop of { fin : expr
              ; block : block
              ; pos : Lexing.position}
  and block = instr list
  type def =
  | Func of { type_: name;
                    name: name ;
                    param: expr list;
                    block: block;
                    pos: Lexing.position }
  and prog = def list
end

module IR = struct
  type name = string
  type expr =
    | Int of int
    | Var of name
    | Call of name * expr list
  type instr = 
    | Decl  of name
    | Assign of name * expr
    | Cond of expr * block * block
    | Loop of expr * block
  and block = instr list
  type def =
    | Func of name * name list * block
  type prog = def list

  let string_of_ir ast =
    let rec fmt_e = function
      | Int n       -> "Int " ^ (string_of_int n)
      | Var v       -> "Var \"" ^ v ^ "\""
      | Call (f, a) -> "Call (\"" ^ f ^ "\", [ "
                       ^ (String.concat " ; " (List.map fmt_e a))
                       ^ " ])"
    and fmt_i = function
      | Decl v -> "Declare (\"" ^ v ^ "\")"
      | Assign (v, e) -> "Assign (\"" ^ v ^ "\", " ^ (fmt_e e) ^ ")"
      | Cond (i,bi,be) -> "Cond (\"" ^ (fmt_e i) ^ "\", [ "
                                    ^ (String.concat " ; " (List.map fmt_i bi))
                                    ^ " ]) else , [ "
                                    ^ (String.concat " ; " (List.map fmt_i be))
                                    ^ " ])"
      | Loop (f,b) ->"Loop (\"" ^ (fmt_e f) ^ "\", [ "
                                ^ (String.concat " ; " (List.map fmt_i b))
                                ^ " ])"
    and fmt_b b = "[ " ^ (String.concat "\n; " (List.map fmt_i b)) ^ " ]"
    in fmt_b ast
end
