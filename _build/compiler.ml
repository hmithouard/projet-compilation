open Ast.IR
open Mips
open Baselib

module Env = Map.Make(String)


type cinfo = { code: Mips.instr list
             ; env: Mips.loc Env.t
             ; fpo: int
             ; counter: int
             ; return: string }
let rec compile_expr e env=
  match e with
  | Int n  -> [ Li (V0, n) ]
  | Var v  -> [ Lw (V0,Env.find v env)]
  | Call (f, args) ->
    let ca = List.map (fun a ->
                 compile_expr a env
                 @ [ Addi (SP, SP, -4)
                   ; Sw (V0, Mem (SP, 0)) ])
               args in
    List.flatten ca
    @ [ Jal f
      ; Addi (SP, SP, 4 * (List.length args)) ]
let rec compile_instr i info =
    match i with
    | Decl v -> 
      let new_fpo = info.fpo + 4 in 
      { info with 
        env = Env.add v (Mem (FP, info.fpo)) info.env
      ; fpo = new_fpo } 
    | Assign (v,e) -> 
        { info with 
        code = info.code 
            @ compile_expr e info.env 
            @ [ Sw (V0, Env.find v info.env) ] }
    | Cond (c, t, e) ->
        let uniq = string_of_int info.counter in
        let ct = compile_block t { info with code = []
                                            ; counter = info.counter + 1 } in
        let ce = compile_block e { info with code = []
                                            ; counter = ct.counter } in
        { info with
          code = info.code
                  @ compile_expr c info.env
                  @ [ Li (T1, 0)]
                  @ [ Beq (V0,T1, "else" ^ uniq  ) ]
                  @ ct.code
                  @ [ B ("endif" ^ uniq)
                    ; Label ("else" ^ uniq) ]
                  @ ce.code
                  @ [ Label ("endif" ^ uniq) ]
        ; counter = ce.counter }
    | Loop (f,b) ->
        let uniq = string_of_int info.counter in
        let cb = compile_block b { info with code = []
                                            ; counter = info.counter + 1} in
        { info with
          code = info.code 
                  @ [Label ("while"^ uniq)] 
                  @ cb.code
                  @ [Li (T0, 0)]
                  @ compile_expr f info.env 
                  @ [Beq (V0,T0,"while"^ uniq)]
        ; counter = cb.counter} 
and compile_block b info =
  match b with
  | [] -> info
  | i :: r ->
     compile_block r (compile_instr i info)

let compile_def (Func (name, args, b)) counter =
    let cb = compile_block b
               { code = []
               ; env =  List.fold_left
                          (fun e (i, a) -> Env.add a (Mem (FP, 4 * i)) e)
                          Env.empty (List.mapi (fun i a -> i + 1, a) args)
               ; fpo = 8
               ; counter = counter + 1
               ; return = "ret" ^ (string_of_int counter) }
    in cb.counter,
       []
       @ [ Label name
         ; Addi (SP, SP, -cb.fpo)
         ; Sw (RA, Mem (SP, cb.fpo - 4))
         ; Sw (FP, Mem (SP, cb.fpo - 8))
         ; Addi (FP, SP, cb.fpo - 4) ]
       @ cb.code
       @ [ Label cb.return
         ; Addi (SP, SP, cb.fpo)
         ; Lw (RA, Mem (FP, 0))
         ; Lw (FP, Mem (FP, -4))
         ; Jr (RA) ]
  
  let rec compile_prog p counter =
    match p with
    | [] -> []
    | d :: r ->
       let new_counter, cd = compile_def d counter in
       cd @ (compile_prog r new_counter)
  
       let compile code =
        let cc = compile_block code {code = [] (* -info.fpo ???? *)
                                ; env = Env.empty
                                ; fpo = 0
                                ; counter = 0
                                ; return = "ret 0" }in
        { text = Baselib.builtins @ cc.code 
        ; data = [("nl", Asciiz "\\n")] }