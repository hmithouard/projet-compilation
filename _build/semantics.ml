open Ast
open Ast.IR
open Baselib

exception Error of string * Lexing.position

let expr_pos expr =
  match expr with
  | Syntax.Int n  -> n.pos
  | Syntax.Var v  -> v.pos
	| Syntax.Call c  -> c.pos
let errt expected given pos =
  raise (Error (Printf.sprintf "expected %s but given %s"
                  (string_of_type_t expected)
                  (string_of_type_t given),
                pos))
let rec analyze_expr expr env =
  match expr with
  | Syntax.Int n -> Int n.value, Int_t
  | Syntax.Var v -> if Env.mem v.name env then
    Var v.name, Env.find v.name env
  else
    raise (Error (Printf.sprintf "unbound variable '%s'" v.name,
                  v.pos))
  | Syntax.Call c ->
     match Env.find_opt c.func env with
      | Some(Func_t (rt, at))->
        if List.length at != List.length c.args then
            raise(Error (Printf.sprintf "expected %d arguments but given  `%d`" (List.length c.args) (List.length at), c.pos))	;
        let args = List.map2 (fun ea a -> 
          let aa, at = analyze_expr a env in 
            if at = ea then 
                aa 
            else 
              errt ea at (expr_pos a) 
              ) at c.args in 
                let aa_t = List.map (fun a ->  analyze_expr a env ) c.args in
                 if at = List.map snd aa_t  then
                      Call( c.func, args ), rt
                 else
                  raise(Error (Printf.sprintf "Pas du bon `%s`" c.func, c.pos ))
                
        | Some(_) -> raise(Error (Printf.sprintf "`%s` is not a function" c.func, c.pos ))
        | None -> raise(Error (Printf.sprintf "unbound function `%s`" c.func, c.pos ))
let rec analyze_instr instr env =
  match instr with
  | Syntax.Decl d -> Decl d.var, (Env.add d.var Int_t env)
  | Syntax.Assign a ->
		if Env.mem a.var env then
			let ae, et = analyze_expr a.expr env in
			let vt = Env.find a.var env in
			if  vt = et then 	
				(Assign (a.var, ae)), env 
			else 
				errt vt et (expr_pos a.expr) 
		else
			raise(Error (Printf.sprintf "unbound variable `%s`" a.var, a.pos ))
  | Syntax.Cond c ->
        let e, et = analyze_expr c.cond env in
        let block_if = analyze_block c.bif env in
        let block_else = analyze_block c.belse env in
          (Cond(e,block_if,block_else)), env
  | Syntax.Loop l ->
    let e, et = analyze_expr l.fin env in
    let b = analyze_block l.block env in
    (Loop(e,b)),env
and analyze_block block env =
  match block with
  | [] -> []
  | instr :: rest ->
     let ai, new_env = analyze_instr instr env in
     ai :: (analyze_block rest new_env)
let analyze parsed =
  analyze_block parsed Baselib._types_
