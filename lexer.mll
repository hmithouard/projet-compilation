{
  open Lexing
  open Parser

  exception Error of char
}

let alpha = ['a'-'z' 'A' - 'Z']
let num = ['0'-'9']
let identifier = alpha(alpha | num | '-' | '_')*

rule token = parse
| eof             { Lend }
| [ ' ' '\t' ]    { token lexbuf }
| '\n'            { Lexing.new_line lexbuf; token lexbuf }
| '+'             { Ladd }
| '*'             { Lmul }
| '-'             { Lsub }
| "ribbif"        { Lif }
| "ribbelse"      { Lelse }
| '('             { Lpo }
| ')'             { Lpf }
| '{'             { Po }
| '}'             { Pf }
| '='             { Lassign }
| "ribbit"        { Ldecl }
| "ribbwhile"     { Lloop }
| "!="            { Lneqq }
| "=="            { Leqq }
| '<'             { Llt }
| '>'             { Lgt }
| identifier as c { Lvar (c) }
| num+ as n       { Lint (int_of_string n) }
| _ as c          { raise (Error c) }
